-- This schema is in PostGreSQL dialect
-- Most of the data is to be found in the match API, but some fields come from the summoner API (3 fields in table player, check my comments there).

drop schema public cascade;
create schema public;

-- enums
create type lane_enum as enum ('MID', 'MIDDLE', 'TOP', 'JUNGLE', 'BOT', 'BOTTOM'); -- used in type participant_timeline
create type lane_type_enum as enum ('BOT_LANE', 'MID_LANE', 'TOP_LANE'); -- used in table event
create type queue_enum as enum ('TEAM_BUILDER_DRAFT_RANKED_5x5', 'RANKED_SOLO_5x5', 'RANKED_TEAM_3x3', 'RANKED_TEAM_5x5');
create type role_enum as enum ('DUO', 'NONE', 'SOLO', 'DUO_CARRY', 'DUO_SUPPORT');
create type season_enum as enum ('PRESEASON3', 'SEASON3', 'PRESEASON2014', 'SEASON2014', 'PRESEASON2015', 'SEASON2015', 'PRESEASON2016', 'SEASON2016');
create type match_mode_enum as enum ('CLASSIC', 'ODIN', 'ARAM', 'TUTORIAL', 'ONEFORALL', 'ASCENSION', 'FIRSTBLOOD', 'KINGPORO');
create type match_type_enum as enum ('CUSTOM_GAME', 'MATCHED_GAME', 'TUTORIAL_GAME');
create type queue_type_enum as enum ('CUSTOM', 'NORMAL_5x5_BLIND', 'RANKED_SOLO_5x5', 'RANKED_PREMADE_5x5', 'BOT_5x5', 'NORMAL_3x3', 'RANKED_PREMADE_3x3', 'NORMAL_5x5_DRAFT', 'ODIN_5x5_BLIND', 'ODIN_5x5_DRAFT', 'BOT_ODIN_5x5', 'BOT_5x5_INTRO', 'BOT_5x5_BEGINNER', 'BOT_5x5_INTERMEDIATE', 'RANKED_TEAM_3x3', 'RANKED_TEAM_5x5', 'BOT_TT_3x3', 'GROUP_FINDER_5x5', 'ARAM_5x5', 'ONEFORALL_5x5', 'FIRSTBLOOD_1x1', 'FIRSTBLOOD_2x2', 'SR_6x6', 'URF_5x5', 'ONEFORALL_MIRRORMODE_5x5', 'BOT_URF_5x5', 'NIGHTMARE_BOT_5x5_RANK1', 'NIGHTMARE_BOT_5x5_RANK2', 'NIGHTMARE_BOT_5x5_RANK5', 'ASCENSION_5x5', 'HEXAKILL', 'BILGEWATER_ARAM_5x5', 'KING_PORO_5x5', 'COUNTER_PICK', 'BILGEWATER_5x5', 'TEAM_BUILDER_DRAFT_UNRANKED_5x5', 'TEAM_BUILDER_DRAFT_RANKED_5x5');
create type highest_achieved_season_tier_enum as enum ('CHALLENGER', 'MASTER', 'DIAMOND', 'PLATINUM', 'GOLD', 'SILVER', 'BRONZE', 'UNRANKED');
create type ascended_type_enum as enum ('CHAMPION_ASCENDED', 'CLEAR_ASCENDED', 'MINION_ASCENDED');
create type building_type_enum as enum ('INHIBITOR_BUILDING', 'TOWER_BUILDING');
create type event_type_enum as enum ('ASCENDED_EVENT', 'BUILDING_KILL', 'CAPTURE_POINT', 'CHAMPION_KILL', 'ELITE_MONSTER_KILL', 'ITEM_DESTROYED', 'ITEM_PURCHASED', 'ITEM_SOLD', 'ITEM_UNDO', 'PORO_KING_SUMMON', 'SKILL_LEVEL_UP', 'WARD_KILL', 'WARD_PLACED');
create type level_up_type_enum as enum ('EVOLVE', 'NORMAL');
create type monster_type_enum as enum ('BARON_NASHOR', 'BLUE_GOLEM', 'DRAGON', 'RED_LIZARD', 'RIFTHERALD', 'VILEMAW');
create type point_captured_enum as enum ('POINT_A', 'POINT_B', 'POINT_C', 'POINT_D', 'POINT_E');
create type tower_type_enum as enum ('BASE_TURRET', 'FOUNTAIN_TURRET', 'INNER_TURRET', 'NEXUS_TURRET', 'OUTER_TURRET', 'UNDEFINED_TURRET');
create type ward_type_enum as enum ('BLUE_TRINKET', 'SIGHT_WARD', 'TEEMO_MUSHROOM', 'UNDEFINED', 'VISION_WARD', 'YELLOW_TRINKET', 'YELLOW_TRINKET_UPGRADE');
create type region_enum as enum ('BR', 'EUNE', 'EUW', 'JP', 'KR', 'LAN', 'LAS', 'NA', 'OCE', 'PBE', 'RU', 'TR');

-- types
create type participant_timeline_data as (
	tenToTwenty real,
	thirtyToEnd real,
	twentyToThirty real,
	zeroToTen real
);

create type participant_timeline as (
	ancientGolemAssistsPerMinCounts participant_timeline_data,
	ancientGolemKillsPerMinCounts participant_timeline_data,
	assistedLaneDeathsPerMinDeltas participant_timeline_data,
	assistedLaneKillsPerMinDeltas participant_timeline_data,
	baronAssistsPerMinCounts participant_timeline_data,
	baronKillsPerMinCounts participant_timeline_data,
	creepsPerMinDeltas participant_timeline_data,
	csDiffPerMinDeltas participant_timeline_data,
	damageTakenDiffPerMinDeltas participant_timeline_data,
	damageTakenPerMinDeltas participant_timeline_data,
	dragonAssistsPerMinCounts participant_timeline_data,
	dragonKillsPerMinCounts participant_timeline_data,
	elderLizardAssistsPerMinCounts participant_timeline_data,
	elderLizardKillsPerMinCounts participant_timeline_data,
	goldPerMinDeltas participant_timeline_data,
	inhibitorAssistsPerMinCounts participant_timeline_data,
	inhibitorKillsPerMinCounts participant_timeline_data,
	lane lane_enum,
	role role_enum,
	towerAssistsPerMinCounts participant_timeline_data,
	towerKillsPerMinCounts participant_timeline_data,
	towerKillsPerMinDeltas participant_timeline_data,
	vilemawAssistsPerMinCounts participant_timeline_data,
	vilemawKillsPerMinCounts participant_timeline_data,
	wardsPerMinDeltas participant_timeline_data,
	xpDiffPerMinDeltas participant_timeline_data,
	xpPerMinDeltas participant_timeline_data
);

create type participant_stats as (
	assists smallint,
	champLevel smallint,
	combatPlayerScore smallint,
	deaths smallint,
	doubleKills smallint,
	firstBloodAssist boolean,
	firstBloodKill boolean,
	firstInhibitorAssist boolean,
	firstInhibitorKill boolean,
	firstTowerAssist boolean,
	firstTowerKill boolean,
	goldEarned int,
	goldSpent int,
	inhibitorKills smallint,
	item0 smallint,
	item1 smallint,
	item2 smallint,
	item3 smallint,
	item4 smallint,
	item5 smallint,
	item6 smallint,
	killingSprees smallint,
	kills smallint,
	largestCriticalStrike smallint,
	largestKillingSpree smallint,
	largestMultiKill smallint,
	magicDamageDealt int,
	magicDamageDealtToChampions int,
	magicDamageTaken int,
	minionsKilled smallint,
	neutralMinionsKilled smallint,
	neutralMinionsKilledEnemyJungle smallint,
	neutralMinionsKilledTeamJungle smallint,
	nodeCapture smallint,
	nodeCaptureAssist smallint,
	nodeNeutralize smallint,
	nodeNeutralizeAssist smallint,
	objectivePlayerScore smallint,
	pentaKills smallint,
	physicalDamageDealt int,
	physicalDamageDealtToChampions int,
	physicalDamageTaken int,
	quadraKills smallint,
	sightWardsBoughtInGame smallint,
	teamObjective smallint,
	totalDamageDealt int,
	totalDamageDealtToChampions int,
	totalDamageTaken int,
	totalHeal int,
	totalPlayerScore smallint,
	totalScoreRank smallint,
	totalTimeCrowdControlDealt smallint,
	totalUnitsHealed smallint,
	towerKills smallint,
	tripleKills smallint,
	trueDamageDealt int,
	trueDamageDealtToChampions int,
	trueDamageTaken int,
	unrealKills smallint,
	visionWardsBoughtInGame smallint,
	wardsKilled smallint,
	wardsPlaced smallint,
	winner boolean
);

create type position_xy as (
	x int,
	y int
);


-- tables
create table player (
	summonerId int,
	matchHistoryUri varchar(60),
	profileIcon int,
	summonerName varchar(30),
	profileIconId int, -- this is not in the match API, but in the summoner API
	revisionDate timestamp, -- this is not in the match API, but in the summoner API
	summonerLevel int, -- this is not in the match API, but in the summoner API
	PRIMARY KEY(summonerId)
);

create table match (
	mapId int,
	matchCreation bigint,
	matchDuration smallint,
	matchId int,
	matchMode match_mode_enum,
	matchType match_type_enum,
	matchVersion varchar(20),
	platformId varchar(10),
	queueType queue_type_enum,
	region region_enum,
	season season_enum,
	frameInterval int,
	PRIMARY KEY (matchId)
);

create table team (
	matchId int,
	baronKills int,
	dominionVictoryScore smallint,
	dragonKills int,
	firstBaron boolean,
	firstBlood boolean,
	firstDragon boolean,
	firstInhibitor boolean,
	firstRiftHerald boolean,
	firstTower boolean,
	inhibitorKills int,
	riftHeraldKills int,
	teamId int,
	towerKills int,
	vilemawKills int,
	winner boolean,
	PRIMARY KEY (matchId, teamId),
	FOREIGN KEY (matchId) REFERENCES match(matchId)
);

create table participant (
	matchId int,
	championId int,
	highestAchievedSeasonTier highest_achieved_season_tier_enum,
	participantId int,
	spell1Id int,
	spell2Id int,
	teamId int,
	timeline participant_timeline,
	participantStats participant_stats,
	PRIMARY KEY (matchId, participantId),
	FOREIGN KEY (matchId) REFERENCES match(matchId),
	FOREIGN KEY (matchId, teamId) REFERENCES team(matchId, teamId)
);

create table mastery (
	matchId int,
	participantId int,
	masteryId smallint,
	rank smallint,
	PRIMARY KEY (matchId, participantId, masteryId),
	FOREIGN KEY (matchId, participantId) REFERENCES participant(matchId, participantId)
);

create table rune (
	matchId int,
	participantId int,
	runeId smallint,
	rank smallint,
	PRIMARY KEY (matchId, participantId, runeId),
	FOREIGN KEY (matchId, participantId) REFERENCES participant(matchId, participantId)
);

create table participant_identity (
	matchId int,
	participantId int,
	summonerId smallint,
	PRIMARY KEY (matchId, participantId),
	FOREIGN KEY (matchId, participantId) REFERENCES participant(matchId, participantId),
	FOREIGN KEY (summonerId) REFERENCES player(summonerId)
);

create table ban (
	matchId int,
	teamId int,
	championId int,
	pickTurn int,
	PRIMARY KEY (matchId, teamId, championId, pickTurn),
	FOREIGN KEY (matchId) REFERENCES match(matchId),
	FOREIGN KEY (matchId, teamId) REFERENCES team(matchId, teamId)
);

create table event (
	eventId int, -- not in the api, must be generated
	matchId int,
	ascendedType ascended_type_enum,
	buildingType building_type_enum,
	creatorId int,
	eventType event_type_enum,
	itemAfter int,
	itemBefore int,
	itemId int,
	killerId int,
	laneType lane_type_enum,
	levelUpType level_up_type_enum,
	monsterType monster_type_enum,
	participantId int,
	pointCaptured point_captured_enum,
	position position_xy,
	skillSlot int,
	teamId int,
	timestamp int,
	towerType tower_type_enum,
	victimId int,
	wardType ward_type_enum,
	PRIMARY KEY (eventId),
	FOREIGN KEY (matchId) REFERENCES match(matchId)
);

create table event_assisting (
	eventId int,
	assistingParticipantId int,
	PRIMARY KEY (eventId, assistingParticipantId),
	FOREIGN KEY (eventId) REFERENCES event(eventId)
);

create table participant_frame  (
	matchId int,
	participantId int,
	currentGold int,
	dominionScore int,
	jungleMinionsKilled int,
	level int,
	minionsKilled int,
	position position_xy,
	teamScore int,
	totalGold int,
	xp int,
	timestamp int,
	PRIMARY KEY (matchId, participantId, timestamp),
	FOREIGN KEY (matchId, participantId) REFERENCES participant(matchId, participantId)
);
