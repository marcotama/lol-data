#!/usr/bin/env bash
archive="`date +"%Y-%m-%d"` - snapshot - LOL data.tar.gz"
tar -zcf "$archive" backup.sh *.json *.py *.sql *.csv .gitignore .git/
for d in ~/backup-destinations/*/ ; do
  echo "[BACKED UP] -> " ${d}
  mkdir -p "${d}LOL-data/"
  cp "$archive" "${d}LOL-data/"
done
rm "$archive"
