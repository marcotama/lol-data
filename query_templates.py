# these are string containing query templates, and can be filled using the .format() function

INSERT_MATCH_QUERY = """
INSERT INTO match VALUES (
    %(mapId)s,
    %(matchCreation)s,
    %(matchDuration)s,
    %(matchId)s,
    %(matchMode)s,
    %(matchType)s,
    %(matchVersion)s,
    %(platformId)s,
    %(queueType)s,
    %(region)s,
    %(season)s,
    %(frameInterval)s
);
"""

INSERT_TEAM_QUERY = """
INSERT INTO team VALUES (
    %(matchId)s,
    %(baronKills)s,
    %(dominionVictoryScore)s,
    %(dragonKills)s,
    %(firstBaron)s,
    %(firstBlood)s,
    %(firstDragon)s,
    %(firstInhibitor)s,
    %(firstRiftHerald)s,
    %(firstTower)s,
    %(inhibitorKills)s,
    %(riftHeraldKills)s,
    %(teamId)s,
    %(towerKills)s,
    %(vilemawKills)s,
    %(winner)s
);
"""

INSERT_PARTICIPANT_QUERY = """
INSERT INTO participant VALUES (
    %(matchId)s,
    %(championId)s,
    %(highestAchievedSeasonTier)s,
    %(participantId)s,
    %(spell1Id)s,
    %(spell2Id)s,
    %(teamId)s,
    (
        (
            %(ancientGolemAssistsPerMinCounts0010)s,
            %(ancientGolemAssistsPerMinCounts1020)s,
            %(ancientGolemAssistsPerMinCounts2030)s,
            %(ancientGolemAssistsPerMinCounts3040)s
        ),
        (
            %(ancientGolemKillsPerMinCounts0010)s,
            %(ancientGolemKillsPerMinCounts1020)s,
            %(ancientGolemKillsPerMinCounts2030)s,
            %(ancientGolemKillsPerMinCounts3040)s
        ),
        (
            %(assistedLaneDeathsPerMinDeltas0010)s,
            %(assistedLaneDeathsPerMinDeltas1020)s,
            %(assistedLaneDeathsPerMinDeltas2030)s,
            %(assistedLaneDeathsPerMinDeltas3040)s
        ),
        (
            %(assistedLaneKillsPerMinDeltas0010)s,
            %(assistedLaneKillsPerMinDeltas1020)s,
            %(assistedLaneKillsPerMinDeltas2030)s,
            %(assistedLaneKillsPerMinDeltas3040)s
        ),
        (
            %(baronAssistsPerMinCounts0010)s,
            %(baronAssistsPerMinCounts1020)s,
            %(baronAssistsPerMinCounts2030)s,
            %(baronAssistsPerMinCounts3040)s
        ),
        (
            %(baronKillsPerMinCounts0010)s,
            %(baronKillsPerMinCounts1020)s,
            %(baronKillsPerMinCounts2030)s,
            %(baronKillsPerMinCounts3040)s
        ),
        (
            %(creepsPerMinDeltas0010)s,
            %(creepsPerMinDeltas1020)s,
            %(creepsPerMinDeltas2030)s,
            %(creepsPerMinDeltas3040)s
        ),
        (
            %(csDiffPerMinDeltas0010)s,
            %(csDiffPerMinDeltas1020)s,
            %(csDiffPerMinDeltas2030)s,
            %(csDiffPerMinDeltas3040)s
        ),
        (
            %(damageTakenDiffPerMinDeltas0010)s,
            %(damageTakenDiffPerMinDeltas1020)s,
            %(damageTakenDiffPerMinDeltas2030)s,
            %(damageTakenDiffPerMinDeltas3040)s
        ),
        (
            %(damageTakenPerMinDeltas0010)s,
            %(damageTakenPerMinDeltas1020)s,
            %(damageTakenPerMinDeltas2030)s,
            %(damageTakenPerMinDeltas3040)s
        ),
        (
            %(dragonAssistsPerMinCounts0010)s,
            %(dragonAssistsPerMinCounts1020)s,
            %(dragonAssistsPerMinCounts2030)s,
            %(dragonAssistsPerMinCounts3040)s
        ),
        (
            %(dragonKillsPerMinCounts0010)s,
            %(dragonKillsPerMinCounts1020)s,
            %(dragonKillsPerMinCounts2030)s,
            %(dragonKillsPerMinCounts3040)s
        ),
        (
            %(elderLizardAssistsPerMinCounts0010)s,
            %(elderLizardAssistsPerMinCounts1020)s,
            %(elderLizardAssistsPerMinCounts2030)s,
            %(elderLizardAssistsPerMinCounts3040)s
        ),
        (
            %(elderLizardKillsPerMinCounts0010)s,
            %(elderLizardKillsPerMinCounts1020)s,
            %(elderLizardKillsPerMinCounts2030)s,
            %(elderLizardKillsPerMinCounts3040)s
        ),
        (
            %(goldPerMinDeltas0010)s,
            %(goldPerMinDeltas1020)s,
            %(goldPerMinDeltas2030)s,
            %(goldPerMinDeltas3040)s
        ),
        (
            %(inhibitorAssistsPerMinCounts0010)s,
            %(inhibitorAssistsPerMinCounts1020)s,
            %(inhibitorAssistsPerMinCounts2030)s,
            %(inhibitorAssistsPerMinCounts3040)s
        ),
        (
            %(inhibitorKillsPerMinCounts0010)s,
            %(inhibitorKillsPerMinCounts1020)s,
            %(inhibitorKillsPerMinCounts2030)s,
            %(inhibitorKillsPerMinCounts3040)s
        ),
        %(lane)s,
        %(role)s,
        (
            %(towerAssistsPerMinCounts0010)s,
            %(towerAssistsPerMinCounts1020)s,
            %(towerAssistsPerMinCounts2030)s,
            %(towerAssistsPerMinCounts3040)s
        ),
        (
            %(towerKillsPerMinCounts0010)s,
            %(towerKillsPerMinCounts1020)s,
            %(towerKillsPerMinCounts2030)s,
            %(towerKillsPerMinCounts3040)s
        ),
        (
            %(towerKillsPerMinDeltas0010)s,
            %(towerKillsPerMinDeltas1020)s,
            %(towerKillsPerMinDeltas2030)s,
            %(towerKillsPerMinDeltas3040)s
        ),
        (
            %(vilemawAssistsPerMinCounts0010)s,
            %(vilemawAssistsPerMinCounts1020)s,
            %(vilemawAssistsPerMinCounts2030)s,
            %(vilemawAssistsPerMinCounts3040)s
        ),
        (
            %(vilemawKillsPerMinCounts0010)s,
            %(vilemawKillsPerMinCounts1020)s,
            %(vilemawKillsPerMinCounts2030)s,
            %(vilemawKillsPerMinCounts3040)s
        ),
        (
            %(wardsPerMinDeltas0010)s,
            %(wardsPerMinDeltas1020)s,
            %(wardsPerMinDeltas2030)s,
            %(wardsPerMinDeltas3040)s
        ),
        (
            %(xpDiffPerMinDeltas0010)s,
            %(xpDiffPerMinDeltas1020)s,
            %(xpDiffPerMinDeltas2030)s,
            %(xpDiffPerMinDeltas3040)s
        ),
        (
            %(xpPerMinDeltas0010)s,
            %(xpPerMinDeltas1020)s,
            %(xpPerMinDeltas2030)s,
            %(xpPerMinDeltas3040)s
        )
    ),
    (
        %(assists)s,
        %(champLevel)s,
        %(combatPlayerScore)s,
        %(deaths)s,
        %(doubleKills)s,
        %(firstBloodAssist)s,
        %(firstBloodKill)s,
        %(firstInhibitorAssist)s,
        %(firstInhibitorKill)s,
        %(firstTowerAssist)s,
        %(firstTowerKill)s,
        %(goldEarned)s,
        %(goldSpent)s,
        %(inhibitorKills)s,
        %(item0)s,
        %(item1)s,
        %(item2)s,
        %(item3)s,
        %(item4)s,
        %(item5)s,
        %(item6)s,
        %(killingSprees)s,
        %(kills)s,
        %(largestCriticalStrike)s,
        %(largestKillingSpree)s,
        %(largestMultiKill)s,
        %(magicDamageDealt)s,
        %(magicDamageDealtToChampions)s,
        %(magicDamageTaken)s,
        %(minionsKilled)s,
        %(neutralMinionsKilled)s,
        %(neutralMinionsKilledEnemyJungle)s,
        %(neutralMinionsKilledTeamJungle)s,
        %(nodeCapture)s,
        %(nodeCaptureAssist)s,
        %(nodeNeutralize)s,
        %(nodeNeutralizeAssist)s,
        %(objectivePlayerScore)s,
        %(pentaKills)s,
        %(physicalDamageDealt)s,
        %(physicalDamageDealtToChampions)s,
        %(physicalDamageTaken)s,
        %(quadraKills)s,
        %(sightWardsBoughtInGame)s,
        %(teamObjective)s,
        %(totalDamageDealt)s,
        %(totalDamageDealtToChampions)s,
        %(totalDamageTaken)s,
        %(totalHeal)s,
        %(totalPlayerScore)s,
        %(totalScoreRank)s,
        %(totalTimeCrowdControlDealt)s,
        %(totalUnitsHealed)s,
        %(towerKills)s,
        %(tripleKills)s,
        %(trueDamageDealt)s,
        %(trueDamageDealtToChampions)s,
        %(trueDamageTaken)s,
        %(unrealKills)s,
        %(visionWardsBoughtInGame)s,
        %(wardsKilled)s,
        %(wardsPlaced)s,
        %(winner)s
    )
);
"""

INSERT_MASTERY_QUERY = """
INSERT INTO mastery VALUES (
    %(matchId)s,
    %(participantId)s,
    %(masteryId)s,
    %(rank)s
);
"""

INSERT_RUNE_QUERY = """
INSERT INTO rune VALUES (
    %(matchId)s,
    %(participantId)s,
    %(runeId)s,
    %(rank)s
);
"""

INSERT_PLAYER_QUERY = """
INSERT INTO player VALUES (
    %(summonerId)s,
    %(matchHistoryUri)s,
    %(profileIcon)s,
    %(summonerName)s,
    %(profileIconId)s,
    %(revisionDate)s,
    %(summonerLevel)s
) ON CONFLICT DO NOTHING;
"""
# only supported from PostGreSQL 9.5 - https://wiki.postgresql.org/wiki/Apt#PostgreSQL_packages_for_Debian_and_Ubuntu

INSERT_PARTICIPANT_IDENTITY_QUERY = """
INSERT INTO participant_identity VALUES (
    %(matchId)s,
    %(participantId)s,
    %(player)s
) ON CONFLICT DO NOTHING;
"""

INSERT_BAN_QUERY = """
INSERT INTO ban VALUES (
    %(matchId)s,
    %(teamId)s,
    %(championId)s,
    %(pickTurn)s
);
"""

INSERT_PARTICIPANT_FRAME_QUERY = """
INSERT INTO participant_frame VALUES (
    %(matchId)s,
    %(participantId)s,
    %(currentGold)s,
    %(dominionScore)s,
    %(jungleMinionsKilled)s,
    %(level)s,
    %(minionsKilled)s,
    (%(position_x)s, %(position_y)s),
    %(teamScore)s,
    %(totalGold)s,
    %(xp)s,
    %(timestamp)s
);
"""

INSERT_EVENT_QUERY = """
INSERT INTO event VALUES (
    %(eventId)s,
    %(matchId)s,
    %(ascendedType)s,
    %(buildingType)s,
    %(creatorId)s,
    %(eventType)s,
    %(itemAfter)s,
    %(itemBefore)s,
    %(itemId)s,
    %(killerId)s,
    %(laneType)s,
    %(levelUpType)s,
    %(monsterType)s,
    %(participantId)s,
    %(pointCaptured)s,
    (%(position_x)s, %(position_y)s),
    %(skillSlot)s,
    %(teamId)s,
    %(timestamp)s,
    %(towerType)s,
    %(victimId)s,
    %(wardType)s
) RETURNING eventId;
"""

INSERT_EVENT_ASSISTING_QUERY = """
INSERT INTO event_assisting VALUES (
    %(eventId)s,
    %(assistingParticipantId)s
);
"""



SELECT_QUERY = """SELECT * FROM match"""
