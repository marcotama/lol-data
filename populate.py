import json
import psycopg2
import traceback
import tabulate
from collections import defaultdict
import argparse
import getpass
from typing import List
import codecs
import os
import sys
import mimetypes
from query_templates import *

events_counter = 0


def get_new_event_id():
    global events_counter
    events_counter += 1
    return events_counter


def populate(
    db_name: str,
    db_user: str,
    db_password: str,
    db_host: str,
    db_port: int,
    json_files: List[str],
    verbose: bool=True
):
    # create a database named lol - the connection is done to the default DB: postgres
    conn = psycopg2.connect(dbname='postgres', user=db_user, host=db_host, port=db_port, password=db_password)
    conn.autocommit = True  # Has to be done, otherwise DBs cannot be dropped or created
    cursor = conn.cursor()
    cursor.execute("""DROP DATABASE IF EXISTS {db_name}""".format(db_name=db_name))
    cursor.execute("""CREATE DATABASE {db_name}""".format(db_name=db_name))
    if verbose:
        print("Created DB")

    # noinspection PyBroadException
    try:
        # use our connection values to establish a connection
        conn = psycopg2.connect(dbname=db_name, user=db_user, host=db_host, port=db_port, password=db_password)
        conn.autocommit = True  # Has to be done, otherwise DBs cannot be dropped or created
        # create a psycopg2 cursor that can execute queries
        cursor = conn.cursor()

        cursor.execute("""SELECT VERSION()""")
        rows = cursor.fetchall()
        column_names = [desc[0] for desc in cursor.description]
        if verbose:
            print(tabulate.tabulate(rows, headers=column_names))

        # erase whatever is in the database
        cursor.execute(open("schema.sql", "r").read())
        if verbose:
            print("Initialized DB with schema")

        conn.autocommit = False
        i = 1
        match_ids = set()
        for json_file in json_files:
            print("Parsing file %d of %d: %s" % (i, len(json_files), json_file))
            i += 1
            with codecs.open(json_file, 'r', encoding='utf-8', errors='ignore') as f:
                match = json.load(f)
            queries = []
            # insert data in table match
            if match['matchId'] in match_ids:
                print("Match already exists, ignoring: %d" % match['matchId'])
                continue
            match_ids.add(match['matchId'])
            query = cursor.mogrify(INSERT_MATCH_QUERY, {
                'mapId': match['mapId'],
                'matchCreation': match['matchCreation'],
                'matchDuration': match['matchDuration'],
                'matchId': match['matchId'],
                'matchMode': match['matchMode'],
                'matchType': match['matchType'],
                'matchVersion': match['matchVersion'],
                'platformId': match['platformId'],
                'queueType': match['queueType'],
                'region': match['region'],
                'season': match['season'],
                'frameInterval': match['timeline']['frameInterval'] if 'timeline' in match else None
            })
            queries.append(query)  # cursor.execute(query)
            if verbose:
                print("Inserted match with id %d" % match['matchId'])

            # insert data in table team
            for team in match['teams']:
                query = cursor.mogrify(INSERT_TEAM_QUERY, {
                    'matchId': match['matchId'],
                    'baronKills': team['baronKills'],
                    'dominionVictoryScore': team['dominionVictoryScore'],
                    'dragonKills': team['dragonKills'],
                    'firstBaron': team['firstBaron'],
                    'firstBlood': team['firstBlood'],
                    'firstDragon': team['firstDragon'],
                    'firstInhibitor': team['firstInhibitor'],
                    'firstRiftHerald': team['firstRiftHerald'],
                    'firstTower': team['firstTower'],
                    'inhibitorKills': team['inhibitorKills'],
                    'riftHeraldKills': team['riftHeraldKills'],
                    'teamId': team['teamId'],
                    'towerKills': team['towerKills'],
                    'vilemawKills': team['vilemawKills'],
                    'winner': team['winner'],
                })
                queries.append(query)  # cursor.execute(query)
                if verbose:
                    print("Inserted team with id (%d,%d)" % (match['matchId'], team['teamId']))

                if 'bans' in team:
                    for ban in team['bans']:
                        query = cursor.mogrify(INSERT_BAN_QUERY, {
                            'matchId': match['matchId'],
                            'teamId': team['teamId'],
                            'championId': ban['championId'],
                            'pickTurn': ban['pickTurn'],
                        })
                        queries.append(query)  # cursor.execute(query)
                        if verbose:
                            print("Inserted ban with id (%d,%d,%d,%d)" % (match['matchId'], team['teamId'],
                                                                          ban['championId'], ban['pickTurn']))

            # insert data in table participant
            for participant in match['participants']:
                tl = defaultdict(lambda: {
                    'zeroToTen': None,
                    'tenToTwenty': None,
                    'twentyToThirty': None,
                    'thirtyToEnd': None
                }, participant['timeline'])
                stats = defaultdict(lambda: None, participant['stats'])
                query = cursor.mogrify(INSERT_PARTICIPANT_QUERY, {
                    'matchId': match['matchId'],
                    'championId': participant['championId'],
                    'highestAchievedSeasonTier': participant['highestAchievedSeasonTier'],
                    'participantId': participant['participantId'],
                    'spell1Id': participant['spell1Id'],
                    'spell2Id': participant['spell2Id'],
                    'teamId': participant['teamId'],

                    # timeline
                    'lane': tl['lane'],
                    'role': tl['role'],

                    'ancientGolemAssistsPerMinCounts0010': tl['ancientGolemAssistsPerMinCounts']['zeroToTen'],
                    'ancientGolemAssistsPerMinCounts1020': tl['ancientGolemAssistsPerMinCounts']['tenToTwenty'],
                    'ancientGolemAssistsPerMinCounts2030': tl['ancientGolemAssistsPerMinCounts']['twentyToThirty'],
                    'ancientGolemAssistsPerMinCounts3040': tl['ancientGolemAssistsPerMinCounts']['thirtyToEnd'],

                    'ancientGolemKillsPerMinCounts0010': tl['ancientGolemKillsPerMinCounts']['zeroToTen'],
                    'ancientGolemKillsPerMinCounts1020': tl['ancientGolemKillsPerMinCounts']['tenToTwenty'],
                    'ancientGolemKillsPerMinCounts2030': tl['ancientGolemKillsPerMinCounts']['twentyToThirty'],
                    'ancientGolemKillsPerMinCounts3040': tl['ancientGolemKillsPerMinCounts']['thirtyToEnd'],

                    'assistedLaneDeathsPerMinDeltas0010': tl['assistedLaneDeathsPerMinDeltas']['zeroToTen'],
                    'assistedLaneDeathsPerMinDeltas1020': tl['assistedLaneDeathsPerMinDeltas']['tenToTwenty'],
                    'assistedLaneDeathsPerMinDeltas2030': tl['assistedLaneDeathsPerMinDeltas']['twentyToThirty'],
                    'assistedLaneDeathsPerMinDeltas3040': tl['assistedLaneDeathsPerMinDeltas']['thirtyToEnd'],

                    'assistedLaneKillsPerMinDeltas0010': tl['assistedLaneKillsPerMinDeltas']['zeroToTen'],
                    'assistedLaneKillsPerMinDeltas1020': tl['assistedLaneKillsPerMinDeltas']['tenToTwenty'],
                    'assistedLaneKillsPerMinDeltas2030': tl['assistedLaneKillsPerMinDeltas']['twentyToThirty'],
                    'assistedLaneKillsPerMinDeltas3040': tl['assistedLaneKillsPerMinDeltas']['thirtyToEnd'],

                    'baronAssistsPerMinCounts0010': tl['baronAssistsPerMinCounts']['zeroToTen'],
                    'baronAssistsPerMinCounts1020': tl['baronAssistsPerMinCounts']['tenToTwenty'],
                    'baronAssistsPerMinCounts2030': tl['baronAssistsPerMinCounts']['twentyToThirty'],
                    'baronAssistsPerMinCounts3040': tl['baronAssistsPerMinCounts']['thirtyToEnd'],

                    'baronKillsPerMinCounts0010': tl['baronKillsPerMinCounts']['zeroToTen'],
                    'baronKillsPerMinCounts1020': tl['baronKillsPerMinCounts']['tenToTwenty'],
                    'baronKillsPerMinCounts2030': tl['baronKillsPerMinCounts']['twentyToThirty'],
                    'baronKillsPerMinCounts3040': tl['baronKillsPerMinCounts']['thirtyToEnd'],

                    'creepsPerMinDeltas0010': tl['creepsPerMinDeltas']['zeroToTen'],
                    'creepsPerMinDeltas1020': tl['creepsPerMinDeltas']['tenToTwenty'],
                    'creepsPerMinDeltas2030': tl['creepsPerMinDeltas']['twentyToThirty'],
                    'creepsPerMinDeltas3040': tl['creepsPerMinDeltas']['thirtyToEnd'],

                    'csDiffPerMinDeltas0010': tl['csDiffPerMinDeltas']['zeroToTen'],
                    'csDiffPerMinDeltas1020': tl['csDiffPerMinDeltas']['tenToTwenty'],
                    'csDiffPerMinDeltas2030': tl['csDiffPerMinDeltas']['twentyToThirty'],
                    'csDiffPerMinDeltas3040': tl['csDiffPerMinDeltas']['thirtyToEnd'],

                    'damageTakenDiffPerMinDeltas0010': tl['damageTakenDiffPerMinDeltas']['zeroToTen'],
                    'damageTakenDiffPerMinDeltas1020': tl['damageTakenDiffPerMinDeltas']['tenToTwenty'],
                    'damageTakenDiffPerMinDeltas2030': tl['damageTakenDiffPerMinDeltas']['twentyToThirty'],
                    'damageTakenDiffPerMinDeltas3040': tl['damageTakenDiffPerMinDeltas']['thirtyToEnd'],

                    'damageTakenPerMinDeltas0010': tl['damageTakenPerMinDeltas']['zeroToTen'],
                    'damageTakenPerMinDeltas1020': tl['damageTakenPerMinDeltas']['tenToTwenty'],
                    'damageTakenPerMinDeltas2030': tl['damageTakenPerMinDeltas']['twentyToThirty'],
                    'damageTakenPerMinDeltas3040': tl['damageTakenPerMinDeltas']['thirtyToEnd'],

                    'dragonAssistsPerMinCounts0010': tl['dragonAssistsPerMinCounts']['zeroToTen'],
                    'dragonAssistsPerMinCounts1020': tl['dragonAssistsPerMinCounts']['tenToTwenty'],
                    'dragonAssistsPerMinCounts2030': tl['dragonAssistsPerMinCounts']['twentyToThirty'],
                    'dragonAssistsPerMinCounts3040': tl['dragonAssistsPerMinCounts']['thirtyToEnd'],

                    'dragonKillsPerMinCounts0010': tl['dragonKillsPerMinCounts']['zeroToTen'],
                    'dragonKillsPerMinCounts1020': tl['dragonKillsPerMinCounts']['tenToTwenty'],
                    'dragonKillsPerMinCounts2030': tl['dragonKillsPerMinCounts']['twentyToThirty'],
                    'dragonKillsPerMinCounts3040': tl['dragonKillsPerMinCounts']['thirtyToEnd'],

                    'elderLizardAssistsPerMinCounts0010': tl['elderLizardAssistsPerMinCounts']['zeroToTen'],
                    'elderLizardAssistsPerMinCounts1020': tl['elderLizardAssistsPerMinCounts']['tenToTwenty'],
                    'elderLizardAssistsPerMinCounts2030': tl['elderLizardAssistsPerMinCounts']['twentyToThirty'],
                    'elderLizardAssistsPerMinCounts3040': tl['elderLizardAssistsPerMinCounts']['thirtyToEnd'],

                    'elderLizardKillsPerMinCounts0010': tl['elderLizardKillsPerMinCounts']['zeroToTen'],
                    'elderLizardKillsPerMinCounts1020': tl['elderLizardKillsPerMinCounts']['tenToTwenty'],
                    'elderLizardKillsPerMinCounts2030': tl['elderLizardKillsPerMinCounts']['twentyToThirty'],
                    'elderLizardKillsPerMinCounts3040': tl['elderLizardKillsPerMinCounts']['thirtyToEnd'],

                    'goldPerMinDeltas0010': tl['goldPerMinDeltas']['zeroToTen'],
                    'goldPerMinDeltas1020': tl['goldPerMinDeltas']['tenToTwenty'],
                    'goldPerMinDeltas2030': tl['goldPerMinDeltas']['twentyToThirty'],
                    'goldPerMinDeltas3040': tl['goldPerMinDeltas']['thirtyToEnd'],

                    'inhibitorAssistsPerMinCounts0010': tl['inhibitorAssistsPerMinCounts']['zeroToTen'],
                    'inhibitorAssistsPerMinCounts1020': tl['inhibitorAssistsPerMinCounts']['tenToTwenty'],
                    'inhibitorAssistsPerMinCounts2030': tl['inhibitorAssistsPerMinCounts']['twentyToThirty'],
                    'inhibitorAssistsPerMinCounts3040': tl['inhibitorAssistsPerMinCounts']['thirtyToEnd'],

                    'inhibitorKillsPerMinCounts0010': tl['inhibitorKillsPerMinCounts']['zeroToTen'],
                    'inhibitorKillsPerMinCounts1020': tl['inhibitorKillsPerMinCounts']['tenToTwenty'],
                    'inhibitorKillsPerMinCounts2030': tl['inhibitorKillsPerMinCounts']['twentyToThirty'],
                    'inhibitorKillsPerMinCounts3040': tl['inhibitorKillsPerMinCounts']['thirtyToEnd'],

                    'towerAssistsPerMinCounts0010': tl['towerAssistsPerMinCounts']['zeroToTen'],
                    'towerAssistsPerMinCounts1020': tl['towerAssistsPerMinCounts']['tenToTwenty'],
                    'towerAssistsPerMinCounts2030': tl['towerAssistsPerMinCounts']['twentyToThirty'],
                    'towerAssistsPerMinCounts3040': tl['towerAssistsPerMinCounts']['thirtyToEnd'],

                    'towerKillsPerMinCounts0010': tl['towerKillsPerMinCounts']['zeroToTen'],
                    'towerKillsPerMinCounts1020': tl['towerKillsPerMinCounts']['tenToTwenty'],
                    'towerKillsPerMinCounts2030': tl['towerKillsPerMinCounts']['twentyToThirty'],
                    'towerKillsPerMinCounts3040': tl['towerKillsPerMinCounts']['thirtyToEnd'],

                    'towerKillsPerMinDeltas0010': tl['towerKillsPerMinDeltas']['zeroToTen'],
                    'towerKillsPerMinDeltas1020': tl['towerKillsPerMinDeltas']['tenToTwenty'],
                    'towerKillsPerMinDeltas2030': tl['towerKillsPerMinDeltas']['twentyToThirty'],
                    'towerKillsPerMinDeltas3040': tl['towerKillsPerMinDeltas']['thirtyToEnd'],

                    'vilemawAssistsPerMinCounts0010': tl['vilemawAssistsPerMinCounts']['zeroToTen'],
                    'vilemawAssistsPerMinCounts1020': tl['vilemawAssistsPerMinCounts']['tenToTwenty'],
                    'vilemawAssistsPerMinCounts2030': tl['vilemawAssistsPerMinCounts']['twentyToThirty'],
                    'vilemawAssistsPerMinCounts3040': tl['vilemawAssistsPerMinCounts']['thirtyToEnd'],

                    'vilemawKillsPerMinCounts0010': tl['vilemawKillsPerMinCounts']['zeroToTen'],
                    'vilemawKillsPerMinCounts1020': tl['vilemawKillsPerMinCounts']['tenToTwenty'],
                    'vilemawKillsPerMinCounts2030': tl['vilemawKillsPerMinCounts']['twentyToThirty'],
                    'vilemawKillsPerMinCounts3040': tl['vilemawKillsPerMinCounts']['thirtyToEnd'],

                    'wardsPerMinDeltas0010': tl['wardsPerMinDeltas']['zeroToTen'],
                    'wardsPerMinDeltas1020': tl['wardsPerMinDeltas']['tenToTwenty'],
                    'wardsPerMinDeltas2030': tl['wardsPerMinDeltas']['twentyToThirty'],
                    'wardsPerMinDeltas3040': tl['wardsPerMinDeltas']['thirtyToEnd'],

                    'xpDiffPerMinDeltas0010': tl['xpDiffPerMinDeltas']['zeroToTen'],
                    'xpDiffPerMinDeltas1020': tl['xpDiffPerMinDeltas']['tenToTwenty'],
                    'xpDiffPerMinDeltas2030': tl['xpDiffPerMinDeltas']['twentyToThirty'],
                    'xpDiffPerMinDeltas3040': tl['xpDiffPerMinDeltas']['thirtyToEnd'],

                    'xpPerMinDeltas0010': tl['xpPerMinDeltas']['zeroToTen'],
                    'xpPerMinDeltas1020': tl['xpPerMinDeltas']['tenToTwenty'],
                    'xpPerMinDeltas2030': tl['xpPerMinDeltas']['twentyToThirty'],
                    'xpPerMinDeltas3040': tl['xpPerMinDeltas']['thirtyToEnd'],

                    # stats
                    'assists': stats['assists'],
                    'champLevel': stats['champLevel'],
                    'combatPlayerScore': stats['combatPlayerScore'],
                    'deaths': stats['deaths'],
                    'doubleKills': stats['doubleKills'],
                    'firstBloodAssist': stats['firstBloodAssist'],
                    'firstBloodKill': stats['firstBloodKill'],
                    'firstInhibitorAssist': stats['firstInhibitorAssist'],
                    'firstInhibitorKill': stats['firstInhibitorKill'],
                    'firstTowerAssist': stats['firstTowerAssist'],
                    'firstTowerKill': stats['firstTowerKill'],
                    'goldEarned': stats['goldEarned'],
                    'goldSpent': stats['goldSpent'],
                    'inhibitorKills': stats['inhibitorKills'],
                    'item0': stats['item0'],
                    'item1': stats['item1'],
                    'item2': stats['item2'],
                    'item3': stats['item3'],
                    'item4': stats['item4'],
                    'item5': stats['item5'],
                    'item6': stats['item6'],
                    'killingSprees': stats['killingSprees'],
                    'kills': stats['kills'],
                    'largestCriticalStrike': stats['largestCriticalStrike'],
                    'largestKillingSpree': stats['largestKillingSpree'],
                    'largestMultiKill': stats['largestMultiKill'],
                    'magicDamageDealt': stats['magicDamageDealt'],
                    'magicDamageDealtToChampions': stats['magicDamageDealtToChampions'],
                    'magicDamageTaken': stats['magicDamageTaken'],
                    'minionsKilled': stats['minionsKilled'],
                    'neutralMinionsKilled': stats['neutralMinionsKilled'],
                    'neutralMinionsKilledEnemyJungle': stats['neutralMinionsKilledEnemyJungle'],
                    'neutralMinionsKilledTeamJungle': stats['neutralMinionsKilledTeamJungle'],
                    'nodeCapture': stats['nodeCapture'],
                    'nodeCaptureAssist': stats['nodeCaptureAssist'],
                    'nodeNeutralize': stats['nodeNeutralize'],
                    'nodeNeutralizeAssist': stats['nodeNeutralizeAssist'],
                    'objectivePlayerScore': stats['objectivePlayerScore'],
                    'pentaKills': stats['pentaKills'],
                    'physicalDamageDealt': stats['physicalDamageDealt'],
                    'physicalDamageDealtToChampions': stats['physicalDamageDealtToChampions'],
                    'physicalDamageTaken': stats['physicalDamageTaken'],
                    'quadraKills': stats['quadraKills'],
                    'sightWardsBoughtInGame': stats['sightWardsBoughtInGame'],
                    'teamObjective': stats['teamObjective'],
                    'totalDamageDealt': stats['totalDamageDealt'],
                    'totalDamageDealtToChampions': stats['totalDamageDealtToChampions'],
                    'totalDamageTaken': stats['totalDamageTaken'],
                    'totalHeal': stats['totalHeal'],
                    'totalPlayerScore': stats['totalPlayerScore'],
                    'totalScoreRank': stats['totalScoreRank'],
                    'totalTimeCrowdControlDealt': stats['totalTimeCrowdControlDealt'],
                    'totalUnitsHealed': stats['totalUnitsHealed'],
                    'towerKills': stats['towerKills'],
                    'tripleKills': stats['tripleKills'],
                    'trueDamageDealt': stats['trueDamageDealt'],
                    'trueDamageDealtToChampions': stats['trueDamageDealtToChampions'],
                    'trueDamageTaken': stats['trueDamageTaken'],
                    'unrealKills': stats['unrealKills'],
                    'visionWardsBoughtInGame': stats['visionWardsBoughtInGame'],
                    'wardsKilled': stats['wardsKilled'],
                    'wardsPlaced': stats['wardsPlaced'],
                    'winner': stats['winner']
                })
                queries.append(query)  # cursor.execute(query)
                if verbose:
                    print("Inserted participant with id (%d,%d)" % (match['matchId'], participant['participantId']))

                if 'masteries' in participant:
                    for mastery in participant['masteries']:
                        query = cursor.mogrify(INSERT_MASTERY_QUERY, {
                            'matchId': match['matchId'],
                            'participantId': participant['participantId'],
                            'masteryId': mastery['masteryId'],
                            'rank': mastery['rank']
                        })
                        queries.append(query)  # cursor.execute(query)
                        if verbose:
                            print("Inserted mastery with id (%d,%d,%d)" % (match['matchId'], participant['teamId'],
                                                                           mastery['masteryId']))
                if 'runes' in participant:
                    for rune in participant['runes']:
                        query = cursor.mogrify(INSERT_RUNE_QUERY, {
                            'matchId': match['matchId'],
                            'participantId': participant['participantId'],
                            'runeId': rune['runeId'],
                            'rank': rune['rank']
                        })
                        queries.append(query)  # cursor.execute(query)
                        if verbose:
                            print("Inserted rune with id (%d,%d,%d)" % (match['matchId'], participant['teamId'],
                                                                        rune['runeId']))

            for participantIdentity in match['participantIdentities']:
                if 'player' in participantIdentity:
                    pl = participantIdentity['player']
                    query = cursor.mogrify(INSERT_PLAYER_QUERY, {
                        'summonerId': pl['summonerId'],
                        'matchHistoryUri': pl['matchHistoryUri'],
                        'profileIcon': pl['profileIcon'],
                        'summonerName': pl['summonerName'],
                        'profileIconId': None,
                        'revisionDate': None,
                        'summonerLevel': None,
                    })
                    queries.append(query)  # cursor.execute(query)
                    if verbose:
                        print("Inserted participant identity with id (%d,%d,%d)" % (
                            match['matchId'],
                            participantIdentity['participantId'],
                            pl['summonerId']))

            if 'timeline' not in match:
                continue
            for frame in match['timeline']['frames']:
                for participantId, participantFrame in frame['participantFrames'].items():
                    position = participantFrame['position'] if 'position' in participantFrame else {'x': None,
                                                                                                    'y': None}
                    participantFrame = defaultdict(lambda: None, participantFrame)
                    query = cursor.mogrify(INSERT_PARTICIPANT_FRAME_QUERY, {
                        'matchId': match['matchId'],
                        'participantId': participantId,
                        'currentGold': participantFrame['currentGold'],
                        'dominionScore': participantFrame['dominionScore'],
                        'jungleMinionsKilled': participantFrame['jungleMinionsKilled'],
                        'level': participantFrame['level'],
                        'minionsKilled': participantFrame['minionsKilled'],
                        'position_x': position['x'],
                        'position_y': position['y'],
                        'teamScore': participantFrame['teamScore'],
                        'totalGold': participantFrame['totalGold'],
                        'xp': participantFrame['xp'],
                        'timestamp': frame['timestamp']
                    })
                    queries.append(query)  # cursor.execute(query)
                    if verbose:
                        print("Inserted participant frame with id (%d,%d,%d)" % (match['matchId'], int(participantId),
                                                                                 frame['timestamp']))
                if 'events' not in frame:
                    continue
                for event in frame['events']:
                    event = defaultdict(lambda: None, event)
                    position = event['position'] if 'position' in event else {'x': None, 'y': None}
                    eventId = get_new_event_id()
                    query = cursor.mogrify(INSERT_EVENT_QUERY, {
                        'eventId': eventId,
                        'matchId': match['matchId'],
                        'ascendedType': event['ascendedType'],
                        'buildingType': event['buildingType'],
                        'creatorId': event['creatorId'],
                        'eventType': event['eventType'],
                        'itemAfter': event['itemAfter'],
                        'itemBefore': event['itemBefore'],
                        'itemId': event['itemId'],
                        'killerId': event['killerId'],
                        'laneType': event['laneType'],
                        'levelUpType': event['levelUpType'],
                        'monsterType': event['monsterType'],
                        'participantId': event['participantId'],
                        'pointCaptured': event['pointCaptured'],
                        'position_x': position['x'],
                        'position_y': position['y'],
                        'skillSlot': event['skillSlot'],
                        'teamId': event['teamId'],
                        'timestamp': event['timestamp'],
                        'towerType': event['towerType'],
                        'victimId': event['victimId'],
                        'wardType': event['wardType']
                    })
                    queries.append(query)  # cursor.execute(query)
                    if verbose:
                        print("Inserted event with id (%d)" % eventId)

                    if 'assistingParticipantIds' not in event:
                        continue

                    for assistingParticipantId in event['assistingParticipantIds']:
                        query = cursor.mogrify(INSERT_EVENT_ASSISTING_QUERY, {
                            'eventId': eventId,
                            'assistingParticipantId': assistingParticipantId
                        })
                        queries.append(query)  # cursor.execute(query)
                        if verbose:
                            print("Inserted event assisting with id (%d,%d)" % (eventId, assistingParticipantId))

            if i % 50 == 0 or i == len(json_files):
                queries = [str(q, 'utf-8') for q in queries]
                joined_queries = '\n\n'.join(queries)
                try:
                    cursor.execute(joined_queries)
                except psycopg2.DataError:
                    print("Failed batch of queries:\n\n%s" % joined_queries, file=sys.stderr)
                    traceback.print_exc(file=sys.stderr)
                    conn.rollback()
                    conn.autocommit = True
                    for query in queries:
                        try:
                            cursor.execute(query)
                        except psycopg2.DataError:
                            print("Culprit query: %s" % query, file=sys.stderr)
                            traceback.print_exc(file=sys.stderr)
                conn.commit()
                queries = []

        # run a SELECT statement and print the results nicely
        cursor.execute(SELECT_QUERY)
        rows = cursor.fetchall()
        column_names = [desc[0] for desc in cursor.description]
        if verbose:
            print(tabulate.tabulate(rows, headers=column_names))

        # run a SELECT statement and export the output to csv
        output_query = """COPY (
                {query}
            ) TO STDOUT WITH CSV HEADER""".format(query=SELECT_QUERY)
        with open('teams.csv', 'w') as f:
            cursor.copy_expert(output_query, f)
        if verbose:
            print("Exported teams table to CSV file")

    except Exception:
        traceback.print_exc()


def get_all_json_files(files_and_dirs: List[str]):
    JSON_MIMETYPES = (
        'application/json',
        'application/x-javascript',
        'text/javascript',
        'text/x-javascript',
        'text/x-json'
    )
    json_files = []
    for file_or_dir in files_and_dirs:
        if os.path.isdir(file_or_dir):
            file_names = os.listdir(file_or_dir)
            file_paths = [os.path.join(file_or_dir, fn) for fn in file_names]
            json_files += get_all_json_files(file_paths)
        else:
            mimetype, _ = mimetypes.guess_type(file_or_dir)
            if mimetype in JSON_MIMETYPES:
                json_files.append(file_or_dir)
    return json_files


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Parse Riot API match-v2.2 json files and populate a PostGreSQL database')
    parser.add_argument(
        '-d',
        dest='db_name',
        type=str,
        default='lol',
        help='database to connect to (default: lol)'
    )
    parser.add_argument(
        '-u',
        dest='db_user',
        type=str,
        default=getpass.getuser(),
        help='username to connect to the database with (default: current user)'
    )
    parser.add_argument(
        '-p',
        dest='db_password',
        type=str,
        default='',
        help='password to connect to the database (default: empty password)'
    )
    parser.add_argument(
        '-m',
        dest='db_host',
        type=str,
        default='localhost',
        help='host where the DBMS is listening (default: localhost)'
    )
    parser.add_argument(
        '-t',
        dest='db_port',
        type=int,
        default=5432,
        help='port on which the DBMS is listening (default: 5432)'
    )
    parser.add_argument(
        '-f',
        dest='json_files_and_dirs',
        type=str,
        nargs='+',
        help='files/directories to parse'
    )
    parser.add_argument(
        '-v',
        dest='verbose',
        action='store_true',
        help='if given, the program will output more details'
    )
    args = parser.parse_args()
    populate(
        db_name=args.db_name,
        db_user=args.db_user,
        db_password=args.db_password,
        db_host=args.db_host,
        db_port=args.db_port,
        json_files=get_all_json_files(args.json_files_and_dirs),
        verbose=args.verbose
    )
