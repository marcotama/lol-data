# This script is in Python 3


# To install Python, I suggest using miniconda on Windows, or anaconda on Linux
# To install the psycopg2 package run `pip install psycopg2`
# To install the tabulate package run `pip install tabulate`



# Quick intro to iterations in Python

# iterating through lists gives you the elements; example:
#      for element in my_list:
# iterating through dictionaries (i.e. hashmaps) gives you keys, not values; example:
#      for key in my_dictionary:
# you can get values instead by calling .values(); example:
#      for value in my_dictionary.values():
# you can get also get both instead by calling .items(); example:
#      for key, value in my_dictionary.items():

# participantFrames is a dictionary, not a list, so beware



# by the way, this is the level of documentation we would love to see ;)


import json
import psycopg2
import traceback
import tabulate

# this file comes from https://oce.api.pvp.net/api/lol/oce/v2.2/match/137956110?includeTimeline=true&api_key=<YOUR-API-KEY>
with open('match.json', 'r') as f:
    match = json.load(f)

# settings for DB connection
DB_NAME = 'lol'
PASSWORD = 'eyg1198r'
DB_USER = 'postgres'
DB_HOST = 'localhost'
DB_PORT = '5432'

# create a database named lol - the connection is done to the default DB: postgres
conn = psycopg2.connect(dbname='postgres', user=DB_USER, host=DB_HOST, port=DB_PORT, password=PASSWORD)
conn.autocommit = True  # Has to be done, otherwise DBs cannot be dropped or created
cursor = conn.cursor()
cursor.execute("""drop database if exists {db_name}""".format(db_name=DB_NAME))
cursor.execute("""create database {db_name}""".format(db_name=DB_NAME))
print("Created DB")


# these are string containing query templates, and can be filled using the .format() function
INSERT_MATCH_QUERY = """insert into match values ({mapId}, {matchCreation}, {matchDuration}, {matchId}, '{matchMode}', '{matchType}', '{matchVersion}', '{platformId}', '{queueType}', '{region}', '{season}', {frameInterval})"""
INSERT_TEAM_QUERY = """insert into team values ({matchId}, {baronKills}, {dominionVictoryScore}, {dragonKills}, {firstBaron}, {firstBlood}, {firstDragon}, {firstInhibitor}, {firstRiftHerald}, {firstTower}, {inhibitorKills}, {riftHeraldKills}, {teamId}, {towerKills}, {vilemawKills}, {winner})"""

try:
    # use our connection values to establish a connection
    conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, host=DB_HOST, port=DB_PORT, password=PASSWORD)
    # create a psycopg2 cursor that can execute queries
    cursor = conn.cursor()
    
    # erase whatever is in the database
    cursor.execute(open("schema.sql", "r").read())
    print("Initialized DB with schema")
    
    # insert data in table match
    cursor.execute(INSERT_MATCH_QUERY.format(
        mapId=match['mapId'],
        matchCreation=match['matchCreation'],
        matchDuration=match['matchDuration'],
        matchId=match['matchId'],
        matchMode=match['matchMode'],
        matchType=match['matchType'],
        matchVersion=match['matchVersion'],
        platformId=match['platformId'],
        queueType=match['queueType'],
        region=match['region'],
        season=match['season'],
        frameInterval=match['timeline']['frameInterval'])
    )
    print("Inserted match with id %d" % match['matchId'])
    
    
    # insert data in table teams
    for team in match['teams']:
        cursor.execute(INSERT_TEAM_QUERY.format(
            matchId=match['matchId'],
            baronKills=team['baronKills'],
            dominionVictoryScore=team['dominionVictoryScore'],
            dragonKills=team['dragonKills'],
            firstBaron=team['firstBaron'],
            firstBlood=team['firstBlood'],
            firstDragon=team['firstDragon'],
            firstInhibitor=team['firstInhibitor'],
            firstRiftHerald=team['firstRiftHerald'],
            firstTower=team['firstTower'],
            inhibitorKills=team['inhibitorKills'],
            riftHeraldKills=team['riftHeraldKills'],
            teamId=team['teamId'],
            towerKills=team['towerKills'],
            vilemawKills=team['vilemawKills'],
            winner=team['winner'],
        ))
        print("Inserted team with id (%d,%d)" % (match['matchId'], team['teamId']))
    
    
    
    
    # Fill all the other tables here
    # ......
    
    
    SELECT_QUERY = """select * from match"""
    # run a SELECT statement and print the results nicely
    cursor.execute(SELECT_QUERY)
    rows = cursor.fetchall()
    column_names = [desc[0] for desc in cursor.description]
    print(tabulate.tabulate(rows, headers=column_names))
    
    # run a SELECT statement and export the output to csv
    outputquery = "COPY ({query}) TO STDOUT WITH CSV HEADER".format(query=SELECT_QUERY)
    with open('teams.csv', 'w') as f:
        cursor.copy_expert(outputquery, f)
    print("Exported teams table to CSV file")
        
except Exception as e:
    traceback.print_exc()



# The output of this script should be as follows

# Created DB
# Initialized DB with schema
# Inserted match with id 137956110
# Inserted team with id (137956110,100)
# Inserted team with id (137956110,200)
  # mapid    matchcreation    matchduration    matchid  matchmode    matchtype     matchversion    platformid    queuetype                      region    season        frameinterval
# -------  ---------------  ---------------  ---------  -----------  ------------  --------------  ------------  -----------------------------  --------  ----------  ---------------
     # 11    1466670768809             2460  137956110  CLASSIC      MATCHED_GAME  6.12.147.611    OC1           TEAM_BUILDER_DRAFT_RANKED_5x5  OCE       SEASON2016            60000
# Exported teams table to CSV file