Get PostGreSQL ready (Debian Jessie)
Run these commands in a shell:
    `sudo echo "deb http://apt.postgresql.org/pub/repos/apt/ jessie-pgdg main" > /etc/apt/sources.list.d/pgdg.list
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install postgresql-9.5
    sudo -u postgres psql postgres`
    
You are in psql now; run:

    `CREATE USER <a-username> WITH PASSWORD '<a-password>' CREATEDB;
    ALTER ROLE <username-chosen-above> Superuser;
    \q`
    
Install the requirements:
    `pip install -r requirements.txt`

To dump the database, run:
    `pg_dump lol > lol.sql`
    
If there is a version mismatch, run:
    `/usr/lib/postgresql/9.5/bin/pg_dump lol > lol.sql`
or use `locate pg_dump` to find the binary for the same version as the dbms (i.e., 9.5)
